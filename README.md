# ECR Tf

Instalacija AWS cli-a i konfiguracija Access ID i secret access key. 

https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html

```bash
AWS configure

Kredencijali se nalaze u Security Credentials na AWS konzoli. 
```

Uraditi git clone u vas direktorijum

```bash
git clone https://gitlab.com/Nelica/ecr-tf.git
```
Podesite sledece varijable:

```bash
variables.tf : Ako zelimo lifecycle policy ( Brise docker image starije od 7 dana koje nisu tagovane )
```

Nakon uspesne konfiguracije, koristite:

```bash
terraform init 
terraform plan
terraform apply
```

```bash
Skripta pita za ime repositorya koji ce kreirati.
```


