{
    "rules": [
        {
            "rulePriority": 1,
            "description": "Clean up",
            "selection": {
                "tagStatus": "untagged",
                "countType": "sinceImagePushed",
                "countNumber": 7,
                "countUnit": "days"
            },
            "action": {
                "type": "expire"
            }
        }
    ]
}
