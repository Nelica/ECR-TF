output "arn" {
  value       = aws_ecr_repository.default.arn
  description = "CEO ARN Repository"
}

output "name" {
  value       = aws_ecr_repository.default.name
  description = "Ime repository"
}

output "registry_id" {
  value       = aws_ecr_repository.default.registry_id
  description = "ID gde je kreiran"
}

output "repository_url" {
  value       = aws_ecr_repository.default.repository_url
  description = "Direktan URL ka repo-u"
}
