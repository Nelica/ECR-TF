variable "repository_name" {
  type        = string
  description = "Ime repository"
}

variable "attach_lifecycle_policy" {
  default     = false
  type        = bool
  description = "Ako je uslov true, dodaje lifecycle polisu"
}

variable "lifecycle_policy" {
  default     = ""
  type        = string
  description = "ECR polisa"
}
